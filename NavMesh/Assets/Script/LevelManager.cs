﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public int centcount = 0;
    public GameObject player;
    public Text rd;
    public Text sd;

    public Button nextBtn;

    // Start is called before the first frame update
    void Start()
    {
        nextBtn.gameObject.SetActive(false);
        GetCent();
    }

    // Update is called once per frame
    void Update()
    {
        if (rd != null && sd != null)
        {

            rd.text = "RD: " + player.GetComponent<PlayerController>().d;
            sd.text = "SD: " + player.GetComponent<NavMeshAgent>().stoppingDistance.ToString();
        }

        if (centcount == player.GetComponent<PlayerController>().centCount)
        {
            nextBtn.gameObject.SetActive(true);
        }
    }

    public void ToScene(int i)
    {
        SceneManager.LoadScene(i);
    }
    public void GetCent()
    {
        centcount = GameObject.FindGameObjectsWithTag("Cent").Length;
    }
}
