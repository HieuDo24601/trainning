﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public GameObject level1Ground;
    public GameObject level2Ground;
    public GameObject level3Ground;

    public GameObject level1Cam;
    public GameObject level2Cam;
    public GameObject level3Cam;

    public GameObject level1Player;
    public GameObject level2Player;
    public GameObject level3Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
