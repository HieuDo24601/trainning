﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class LevelGenerator : MonoBehaviour
{
    public NavMeshSurface navMesh;

    public int width = 10;
    public int height = 10;

    public GameObject wall;
    public GameObject player;
    public GameObject cent;
    public Camera mainCam;

    public GameObject LevelManager;
    GameObject p;

    public Text rd;
    public Text sd;


    private bool playerSpawned = false;

    // Use this for initialization
    void Start()
    {
        GenerateLevel();

        navMesh.BuildNavMesh();

        LevelManager.GetComponent<LevelManager>().player = p;
    }

    // Create a grid based level
    void GenerateLevel()
    {
        // Loop over the grid
        for (int x = 0; x <= width; x += 2)
        {
            for (int y = 0; y <= height; y += 2)
            {
                // Should we place a wall?
                if (Random.value > .7f)
                {
                    // Spawn a wall
                    Vector3 pos = new Vector3(x - width / 2f, 0.3f, y - height / 2f);
                    Instantiate(wall, pos, Quaternion.identity, transform);
                }
                else if (!playerSpawned) // Should we spawn a player?
                {
                    // Spawn the player
                    Vector3 pos = new Vector3(x - width / 2f, 0.04f, y - height / 2f);
                    p = Instantiate(player, pos, Quaternion.identity);
                    p.GetComponent<PlayerController>().mainCam = mainCam;
                    playerSpawned = true;
                }
                else
                {
                    Vector3 pos = new Vector3(x - width / 2f, 1f, y - height / 2f);
                    Instantiate(cent, pos, Quaternion.identity);
                    LevelManager.GetComponent<LevelManager>().centcount++;
                }
            }
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        rd.text = "RD: " + p.GetComponent<NavMeshAgent>().remainingDistance.ToString();
        sd.text = "SD: " + p.GetComponent<NavMeshAgent>().stoppingDistance.ToString();
    }
}
