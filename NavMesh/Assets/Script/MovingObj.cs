﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObj : MonoBehaviour
{
    public int direction = 1;
    public float speed = 5;
    public float time = 0;
    public float lenght = 0.5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime * speed;
        this.transform.position += this.transform.TransformDirection(Vector3.forward) * direction * Mathf.Sin(time) * lenght;
    }
}
