﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public Camera mainCam;
    Vector3 destination;
    public float d = 0;
    bool jump = true;
    public int centCount = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                switch (hit.collider.gameObject.tag)
                {
                    case "Ground":
                        {
                            this.GetComponent<NavMeshAgent>().ResetPath();
                            this.GetComponent<NavMeshAgent>().SetDestination(hit.point);
                            destination = hit.point;
                            break;
                        }
                    case "Cent":
                        {
                            RaycastHit hit2;
                            if (Physics.Raycast(hit.collider.gameObject.transform.position, Vector3.down, out hit2, 200))
                            {
                                this.GetComponent<NavMeshAgent>().ResetPath();
                                this.GetComponent<NavMeshAgent>().SetDestination(hit2.point);
                                destination = hit.point;
                            }
                            break;
                        }
                }

                //this.GetComponent<NavMeshAgent>().SetDestination(hit.point);
            }

        }
        this.GetComponent<NavMeshAgent>().autoTraverseOffMeshLink = false;
        // if (this.GetComponent<NavMeshAgent>().remainingDistance > this.GetComponent<NavMeshAgent>().stoppingDistance)

        d = 0;
        if (this.GetComponent<NavMeshAgent>().pathPending == true)
        {
            //Debug.Log("Des: " + destination.ToString());
            // dis = (int)Vector3.Distance(this.transform.position, destination);
            d = distance(this.transform.position, destination);
            Debug.Log(d);

        }
        else
        {
            d = (int)(this.GetComponent<NavMeshAgent>().remainingDistance * 10);
        }

        bool c = d > 0;
        Debug.Log(c.ToString() + " " + d);

        if (c == true || this.GetComponent<NavMeshAgent>().velocity.magnitude > 1)
        {
            Debug.Log("Walk");
            this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsWalking", true);
            this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsJumping", false);
            //jump=false;
        }
        else
        {
            Debug.Log("Stand");
            this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsWalking", false);
            this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsJumping", false);
            //this.transform.position = this.GetComponent<NavMeshAgent>().pathEndPosition;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        if (this.GetComponent<NavMeshAgent>().isOnOffMeshLink == true)
        {
            if (jump == true)
            {
                this.GetComponent<NavMeshAgent>().isStopped = true;
                Debug.Log("Jump");
                this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsWalking", true);
                this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsJumping", true);
                //if (startJump == true)
                {
                    StartCoroutine(ParabolaJump(this.GetComponent<NavMeshAgent>(), 6, 1f));
                }
            }
        }
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Cent":
                {
                    centCount++;
                    GameObject.Destroy(other.gameObject);
                    break;
                }
        }
    }

    IEnumerator ParabolaJump(NavMeshAgent agent, float height, float duration)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float normalizedTime = 0.0f;
        while (normalizedTime < 1.0f)
        {
            float yOffset = height * 2.0f * (normalizedTime - normalizedTime * normalizedTime);
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
            normalizedTime += Time.deltaTime / (duration);
            jump = false;
            this.transform.GetChild(0).GetComponent<Animator>().SetBool("IsJumping", true);
            yield return null;
        }

        this.GetComponent<NavMeshAgent>().isStopped = false;
        agent.CompleteOffMeshLink();
        jump = true;
        //agent.ResetPath();
        //agent.SetDestination(destination);
        StopCoroutine(ParabolaJump(agent, height, duration));

        Debug.Log("End Jump");
    }

    float distance(Vector3 a, Vector3 b)
    {
        return Mathf.Sqrt(
            Mathf.Pow((a.x - b.x), 2) +
            Mathf.Pow((a.y - b.y), 2) +
            Mathf.Pow((a.z - b.z), 2)
        );
    }
}
