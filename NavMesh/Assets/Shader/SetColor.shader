﻿Shader "Unlit/SetColor"
{
    Properties
    {
        _MainColor("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Overlay"
            "RenderType" = "Opaque"
        }

        CGPROGRAM

        #pragma surface surf Standard

        half4 _MainColor;

        struct Input
        {
            float2 uv_MainTex : TEXCOORD0;
        };

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            o.Albedo = _MainColor;
        }

        ENDCG
    }

    Fallback "Diffuse"
}
