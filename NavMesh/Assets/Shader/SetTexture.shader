﻿Shader "Unlit/SetTexture"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "White" {}
        [NoScaleOffset]_NormalMap("Normal map", 2D) = "bump" {}
        _NormalStrength("Strength", Range(0,5)) = 0.5
    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
        }

        CGPROGRAM

        #pragma surface surf Standard

        sampler2D _MainTex;
        sampler2D _NormalMap;

        float _NormalStrength;

        struct Input
        {
            float2 uv_MainText : TEXCOORD0;
        };

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            o.Albedo = tex2D(_MainTex, IN.uv_MainText);

            half4 normal = tex2D(_NormalMap, IN.uv_MainText);
            half3 unpack = UnpackNormal(normal);
            unpack.xy *= _NormalStrength;

            o.Normal = normalize(unpack);
        } 

        ENDCG
    }

    Fallback "Diffuse"
}
