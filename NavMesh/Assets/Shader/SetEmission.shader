﻿Shader "Unlit/SetEmission"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "White"{}
        _MainCol("Main Color", Color) = (1,1,1,1)

        _EmissionTex("Emission Texture", 2D) = "White" {}
        _EmissionCol("Emission Color", Color) = (1,1,1,1)
        _EmissionStr("Emission Strength", Range(0,10))=1
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
        }

        CGPROGRAM

        #pragma surface surf Standard
        
        sampler2D _MainTex;
        half4 _MainCol;

        sampler2D _EmissionTex;
        half4 _EmissionCol;
        half _EmissionStr;

        struct Input
        {
            float2 uv_MainTex: TEXCOORD0;
        };

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            o.Albedo = tex2D(_MainTex, IN.uv_MainTex) * _MainCol;

            o.Emission = _EmissionStr  * _EmissionCol;
        }

        ENDCG
    }

    Fallback "Diffuse"
}
